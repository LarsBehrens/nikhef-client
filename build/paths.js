var appRoot = 'src/';
var outputRoot = 'dist/';
var exportSrvRoot = 'export/';

module.exports = {
  root: appRoot,
  source: appRoot + '**/*.js',
  html: appRoot + '**/*.html',
  css: appRoot + '**/*.css',
  style: appRoot + 'styles/**/*.styl',
  styleIndex: appRoot + 'styles/index.styl',
  output: outputRoot,
  exportSrv: exportSrvRoot,
};
