export class Config {
	
	constructor() {
		this.apiUrl = 'http://localhost:3000';
	}

	get(key) {
		return this[key] || undefined;
	}
}