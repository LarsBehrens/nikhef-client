import {inject} from 'aurelia-framework';

import pf from 'babel-polyfill';

export class App {

  configureRouter(config, router) {
    // config.title = 'Aurelia';
    config.map([
      { route: ['', 'welcome'], name: 'welcome',      moduleId: 'pages/welcome/welcome',      nav: true, title: 'Welcome' },
      { route: 'map', name: 'map', moduleId: 'pages/map/map', nav:true, title: 'Map'},
      { route: 'activity', name: 'activity', moduleId: 'pages/activity/activity', nav:true, title: 'Activity'},
      { route: 'host', name: 'host', moduleId: 'pages/host/host', nav:true, title: 'Host'},
      ]);


    this.router = router;
  }
}
