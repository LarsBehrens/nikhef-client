import Moment from 'moment';

export class DateFormatValueConverter {
  toView(value) {
    return Moment(value).format('H:mm:ss M/D/YYYY');
  }
}