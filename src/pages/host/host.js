import D3 from 'd3';
import Axios from 'axios';

export class Host {

	constructor() {
		this.baseUrl = 'http://localhost:3000';
		this.from = "2016-05-26T14:15:27.013Z";
		this.to = "2016-05-27T14:15:27.013Z";
		this.interval = 'hour';
		//click action 0 zoom, 1 details
		this.clickAction = 0;
		this.hostIndex;
		this.hosts;
		this.state = 0; // 0 = overview, 1=zoom, 2=dod.
	}

	async attached() {
		this.load();
	}

	select(action) {
		this.clickAction = action;
	}

	back() {
		this.state = 0;
		this.load();
	}

	async load(action) {
		console.log(action);
		if (action === 0 || this.state === 1) {
			this.state = 1;
			D3.select('#hostgraph').html('');
			this.renderActivity();
		} else if (action === 1 || this.state === 2) {
			this.state = 2;
			D3.select('#hostgraph').html('');
			await this.loadDestData();
			this.renderDestBubbles();
		} else {
			D3.select('#hostgraph').html('');
			await this.loadData();
			this.renderBubbles();
		}
	}

	// of boolean of data meegeven. not sure!
	renderBubbles(details) {
		let counter = 0;
		const diameter = 710;
		const width = 670;
		const height = 670;
		const colour = D3.scale.category20();
		const colourB = D3.scale.category20b();

		const bubble = D3.layout.pack()
			.sort(null)
			.size([width, height])
			.padding(1.5);

		// D3.select('#bubbles').html('');
		const svg = D3.select('#hostgraph').append('svg')
			.attr('width', width)
			.attr('height', height)
			.attr('class', 'bubbleSvg')

		console.log(this.root);

		var tooltip = d3.select("#hostgraph").append("div")
			.attr("class", "atooltip")
			.style("opacity", 0);

		const node = svg.selectAll(".node")
			.data(bubble.nodes(this.root)
				.filter(function(d) {
					return !d.children;
				}))
			.enter()
			.append("g")
			.attr("class", "node")
			.attr("transform", function(d) {
				return "translate(" + d.x + "," + d.y + ")";
			});

		node.append("circle")
			.attr("r", function(d) {
				return d.r;
			})
			.style("fill", (d) => {
				counter += 1;
				console.log(counter);
				if (counter < 20) {
					return colour(d.name);
				} else {
					return colourB(d.name);
				}
			});

		node.append("text")
			.attr("dy", ".3em")
			.style("text-anchor", "middle")
			.text(function(d) {
				return d.name;
			});

		node.on('mouseover', (d) => {
			tooltip.html('hostname: ' + d.name + '<br/> ip: ' + d.ip + '<br/> frequency: ' + d.value);
			tooltip
			.style('z-index', 999);
			tooltip.transition()
				.duration(200)
				.style('opacity', 0.9)
				.style('top', d.y + 'px')
				.style('left', (d.x + 450) + 'px');
		});

		node.on('mouseout', (d) => {
			tooltip.html();
			tooltip.transition()
				.duration(200)
				.style('opacity', 0)
			tooltip
			.style('z-index', -1);
		});

		node.on('click', (d) => {
			this.hostIndex = d.index;
			this.load(this.clickAction);
		});
	}

	renderDestBubbles(details) {
		let counter = 0;
		const diameter = 710;
		const width = 670;
		const height = 670;
		const colour = D3.scale.category20();
		const colourB = D3.scale.category20b();

		const bubble = D3.layout.pack()
			.sort(null)
			.size([width, height])
			.padding(1.5);

		// D3.select('#bubbles').html('');
		const svg = D3.select('#hostgraph').append('svg')
			.attr('width', width)
			.attr('height', height)
			.attr('class', 'bubbleSvg')

		console.log(this.destRoot);

		var tooltip = d3.select("#hostgraph").append("div")
			.attr("class", "atooltip")
			.style("opacity", 0)
			.style("z-index", -1);

		const node = svg.selectAll(".node")
			.data(bubble.nodes(this.destRoot)
				.filter(function(d) {
					return !d.children;
				}))
			.enter()
			.append("g")
			.attr("class", "node")
			.attr("transform", function(d) {
				return "translate(" + d.x + "," + d.y + ")";
			});

		node.append("circle")
			.attr("r", function(d) {
				return d.r;
			})
			.style("fill", (d) => {
				counter += 1;
				console.log(counter);
				if (counter < 20) {
					return colour(d.name);
				} else {
					return colourB(d.name);
				}
			});

		node.append("text")
			.attr("dy", ".3em")
			.style("text-anchor", "middle")
			.text(function(d) {
				return d.name;
			});

		node.on('mouseover', (d) => {
			tooltip.html('hostname: ' + d.name + '<br/> frequency: ' + d.value);
			tooltip
			.style('z-index', 999);
			tooltip.transition()
				.duration(200)
				.style('opacity', 0.9)
				.style('top', d.y + 'px')
				.style('left', (d.x + 450) + 'px')
		});

		node.on('mouseout', (d) => {
			tooltip.html();
			tooltip.transition()
				.duration(200)
				.style('opacity', 0)
			tooltip
			.style('z-index', -1);
		});

	}

	async loadData() {
		// const url = `${this.baseUrl}/es/transfer/activity/fs?from=${this.from}&to=${this.to}`;
		const url = `${this.baseUrl}/es/transfer/activity?from=${this.from}&to=${this.to}&interval=${this.interval}`;
		const response = await Axios.get(url);
		this.root = {
			children: []
		}
		this.data = response.data;
		this.hosts = response.data.map(function(element) {
			return element.key;
		});
		let index = 0;
		response.data.map((elem) => {
			const hostname = this.ipToHost(elem.key);
			this.root.children.push({
				ip: elem.key,
				name: hostname,
				value: elem.doc_count,
				index: index
			});
			// 		this.root.children.push({
			// 	name: elem.key,
			// 	value: elem.size_agg.value,
			// 	index: index
			// });
			index++;
		});
	}

	async loadDestData() {
		console.log(this.hosts);
		console.log(this.hostIndex);
		const url = `${this.baseUrl}/es/transfer/activity/${this.hosts[this.hostIndex]}/dest?from=${this.from}&to=${this.to}&interval=${this.interval}`
		const response = await Axios.get(url);
		this.destRoot = {
			children: []
		}

		for (var i in response.data) {
			if (i == 30) {
				break;
			}
			this.destRoot.children.push({
				name: response.data[i].key,
				value: response.data[i].doc_count
			});
		}
		console.log(this.destRoot);
	}

	renderActivity() {
		const data = this.data[this.hostIndex].per_hour_agg.buckets;
		console.log(data);

		const margin = {
			top: 20,
			right: 20,
			bottom: 25,
			left: 60
		};
		const width = 670 - margin.left - margin.right;
		const height = 450 - margin.top - margin.bottom;

		// Over welke lengte moeten de bars verspreid worden
		const x = D3.scale.ordinal()
			.rangeRoundBands([0, width], .1);


		// De maximale grootte van een bar gelijk maken aan de lengte 
		const y = D3.scale.linear()
			.range([height, 0]);

		// de x axis wordt aangemaakt
		const xAxis = D3.svg.axis()
			.scale(x)
			.orient("bottom")


		if (this.interval == 'hour') {
			xAxis.tickFormat(D3.time.format("%H"));
		} else if (this.interval == 'minute') {
			xAxis.tickFormat(D3.time.format("%M"));
		}

		// de y axis
		const yAxis = D3.svg.axis()
			.scale(y)
			.orient("left")
			.ticks(10);

		// Hier wordt de buitenste container aangemaakt
		const svg = D3.select("#hostgraph").html("")
			.append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		const parseDate = D3.time.format("%Y-%m-%dT%H:%M:%S.%LZ").parse;

		data.map(function(d) {
			console.log(d.key_as_string);
			d.date = parseDate(d.key_as_string);
			console.log(d.date);
		});

		const barWidth = 450 / data.length + 0.9;

		// temp kleur voor het veranderen van de kleur
		var tempColor;

		// Verschillende soorten
		var colors = D3.scale.linear()
			.domain([0, data.length * .33, data.length * .66, data.length])
			.range(['#B58929', '#C61C6F', '#268BD2', '#85992C'])

		// Hier wil je de letters terug geven als input voor de x as
		x.domain(data.map(function(d) {
			return d.date;
		}));
		// Hier geef je aan de wat de minimale bar is en de grootste
		y.domain([0, D3.max(data, function(d) {
			return d.doc_count;
		})]);

		// Hier wordt de x axis toegevoegd
		svg.append("g")
			.attr("class", "x axis")
			.attr("id", "xaxis")
			.attr("transform", "translate(0," + height + ")")
			.call(xAxis)
			.selectAll("text")
			.attr("y", 0)
			.attr("x", 9)
			.attr("dy", ".35em")
			.attr("transform", "rotate(80)")
			.style("text-anchor", "start");


		// y axis
		svg.append("g")
			.attr("class", "y axis")
			.attr("id", "yaxis")
			.call(yAxis)
			.append("text")
			.attr("transform", "rotate(-90)")
			.attr("y", 6)
			.attr("dy", ".71em")
			.style("text-anchor", "end")
			.text("Amount of transfers");

		// de bars in de chart
		svg.selectAll(".bar")
			.data(data)
			.enter().append("rect")
			.attr("class", "bar")
			.style('fill', function(d, i) {
				return colors(i);
			})
			.attr("x", function(d) {
				return x(d.date);
			})
			.attr("width", barWidth)
			.attr("y", height)
			.attr("height", 0)

		// .on('click', (d) => {
		// 	let newFrom;
		// 	if (this.interval == 'hour') {
		// 		newFrom = D3.time.hour.offset(d.date, 1);
		// 		this.interval = 'minute';
		// 		console.log(this.interval);
		// 	} else if (this.interval == 'minute') {
		// 		// var newDate = D3.time.minute.offset(d.key_as_string, 1);
		// 		// barChart.interval = 'second';
		// 	}

		// 	const formatDate = D3.time.format("%Y-%m-%dT%H:%M:%S.%LZ");
		// 	this.from = formatDate(d.date);
		// 	this.to = formatDate(newFrom);

		// 	this.load(true);
		// })

		.on('mouseover', function(d) {
			tempColor = this.style.fill;
			D3.select(this)
				.style('opacity', .5)
				.style('fill', 'yellow')
		})

		.on('mouseout', function(d) {
			D3.select(this)
				.style('opacity', 1)
				.style('fill', tempColor)
		})

		.transition()
			.attr("y", function(d) {
				return y(d.doc_count);
			})
			.attr("height", function(d) {
				return height - y(d.doc_count);
			})
			.delay(function(d, i) {
				return i * 50;
			})
			.duration(1000)
			.ease('elastic')
		this.loader = false;
	}

	ipToHost(ip) {
		let ipHost;
		switch (ip) {
			case "194.171.96.203":
				ipHost = "oliebol-02";
				break;
			case "194.171.96.210":
				ipHost = "oliebol-09";
				break;
			case "194.171.96.208":
				ipHost = "oliebol-07";
				break;
			case "194.171.96.209":
				ipHost = "oliebol-08";
				break;
			case "194.171.96.205":
				ipHost = "oliebol-04";
				break;
			case "194.171.96.206":
				ipHost = "oliebol-05";
				break;
			case "194.171.96.202":
				ipHost = "oliebol-01";
				break;
			case "194.171.96.204":
				ipHost = "oliebol-03";
				break;
			case "194.171.96.207":
				ipHost = "oliebol-06";
				break;
			case "194.171.96.216":
				ipHost = "haas-01";
				break;
			case "194.171.96.217":
				ipHost = "haas-02";
				break;
			case "194.171.96.173":
				ipHost = "strijker-01";
				break;
			case "194.171.96.190":
				ipHost = "strijker-18";
				break;
			case "194.171.96.188":
				ipHost = "strijker-16";
				break;
			case "194.171.96.191":
				ipHost = "strijker-19";
				break;
			case "194.171.96.174":
				ipHost = "strijker-02";
				break;
			case "194.171.96.189":
				ipHost = "strijker-17";
				break;
			case "194.171.96.176":
				ipHost = "strijker-04";
				break;
			case "194.171.96.175":
				ipHost = "strijker-03";
				break;
			case "194.171.96.178":
				ipHost = "strijker-06";
				break;
			case "194.171.96.177":
				ipHost = "strijker-05";
				break;
			case "194.171.96.187":
				ipHost = "strijker-15";
				break;
			case "194.171.96.179":
				ipHost = "strijker-07";
				break;
			case "194.171.96.182":
				ipHost = "strijker-10";
				break;
			case "194.171.96.180":
				ipHost = "strijker-08";
				break;
			case "194.171.96.184":
				ipHost = "strijker-12";
				break;
			case "194.171.96.183":
				ipHost = "strijker-11";
				break;
			case "194.171.96.186":
				ipHost = "strijker-14";
				break;
			case "194.171.96.181":
				ipHost = "strijker-09";
				break;
			case "194.171.96.185":
				ipHost = "strijker-13";
				break;
			default:
				ipHost = "Unknown Host";
		}
		return ipHost;
	}
}