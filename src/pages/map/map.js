import {inject} from 'aurelia-dependency-injection';
import L from 'leaflet';
import {MapService} from '../../services/mapService';

@inject(MapService)
export class Map {
  hosts;
  hostIndex = 0;
  lMap;
  transferType = 1; // 0 == stores, 1 == retrieves
  coloringType = 1; // 0 == file size, 1 == frequency
  lineWidth = 3;
  refresh = false;
  perNode = false; // if true logs is an array of objects instead of an object of objects.
  loader = false;
  showHosts = false;
  loaded = false;

  constructor(mapService) {
    this.mapService = mapService;

    const now = new Date();
    this.to = now.toISOString();
    now.setHours(now.getHours() - 1);
    this.from = now.toISOString();
  }

  async attached() {
    this.loadMap();
    this.loadLogs();
  }

  loadMap() {
    this.lMap = L.map('map').setView([52.37027, 4.89582], 13);
    const mapLink =
      '<a href="http://openstreetmap.org">OpenStreetMap</a>';
    L.tileLayer(
      'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; ' + mapLink,
        maxZoom: 50,
      }).addTo(this.lMap);

    const urlTemplate = 'https://api.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibGFyc2JlaHJlbnMiLCJhIjoiY2luamloajk0MDA1a3Z2bHl3ajE3YjZmMiJ9.M4KLaN2EEchUJDF6qPNWcA';
    this.lMap.addLayer(L.tileLayer(urlTemplate, {
      minZoom: 4
    }));
  }

  async loadLogs() {
    try {
      this.loader = true;
      this.hosts = await this.mapService.loadLogs(this.from, this.to);
      console.log(this.hosts);
      this.loaded = true;
      this.loader = false;
    } catch (e) {
      console.log(e);
      this.error = 'Error occured!';
    }
  }

  setType(type) {
    switch (type) {
      case 'store':
        this.transferType = 0;
        break;
      case 'retrieve':
        this.transferType = 1;
        break;
      case 'fSize':
        this.coloringType = 0;
        break;
      case 'frequency':
        this.coloringType = 1;
        break;
    }
  }

  async changeHost(hostIndex) {
    this.hostIndex = hostIndex;
    this.drawLines(true);
  }

  async load() {
    try {
      if (!this.loaded || this.refresh) {
        await this.loadLogs();
        this.refresh = false;
      }
      if (!this.loader) {
        await this.loaderOn();
      }
      if (this.perNode) {
        this.drawLines(true);
        this.showHosts = true;
      } else {
        this.showHosts = false;
        this.drawLines();
      }
    } catch (err) {
      console.log(err);
      this.loader = false;
    }
  }


  drawLines(perNode) {
    this.clearMap();

    let logs;
    if (perNode) {
      logs = this.mapService.getHostLogs(this.transferType, this.hostIndex);
    } else {
      logs = this.mapService.getLocLogs(this.transferType);
    }

    this.maxSize = this.mapService.getMaxFileSize(!perNode);
    this.maxFreq = this.mapService.getMaxFreq(!perNode);

    console.log(logs);

    if (perNode) {
      for (const key in logs) {
        this.draw(logs[key]);
      }
    } else {
      logs.map(this.draw.bind(this)); //.bind(this) gives the draw function the 'this' as context.
    }
    this.loader = false;
  }

  draw(elem) {
    const hostIp = elem.host.ip;
    const destIp = elem.destination.ip;
    const destLoc = elem.host.ll;
    const hostLoc = elem.destination.ll;
    const hostName = elem.host.name;
    const frequency = elem.frequency;
    const fileSize = elem.totalSize;
    const avarageSize = (fileSize / frequency);
    let color = '';

    if (this.coloringType === 1) {
      color = this.mapService.calcFreqColor(frequency, this.maxFreq);
    } else {
      color = this.mapService.calcFileSizeColor(fileSize, this.maxSize)
    }

    const polyLineProperties = {
      color: color,
      opacity: 1,
      weight: this.lineWidth,
      clickable: true,
      smoothFactor: 1,
    }

    const popupProperties = {
      maxWidth: 500,
      minWidth: 50,
    }

    const popup = L.popup(popupProperties)
      .setContent('<h4>Host IP: ' + hostIp + '<br />' +
        'Destination IP: ' + destIp + '<br />' +
        'Frequency : ' + frequency + '<br />' +
        'Total amount of data : ' + (fileSize / 1000).toFixed(3) + 'GB' + '<br />' +
        'Average amount of data : ' + avarageSize.toFixed(2) + 'MB' + '</h4>');

    const polyline = L.polyline([
      hostLoc,
      destLoc
    ], polyLineProperties).bindPopup(popup).addTo(this.lMap);
  }


  // trick for showing loader properly.
  async loaderOn() {
    this.loader = true;
    return new Promise((resolve, reject) => {
      setTimeout(function() {
        resolve();
      }, 5);
    });
  }

  clearMap() {
    for (const i in this.lMap._layers) {
      if (this.lMap._layers[i]._path != undefined) {
        try {
          this.lMap.removeLayer(this.lMap._layers[i]);
        } catch (e) {
          console.log('cant remove ' + this.lMap._layers[i]);
        }
      }
    }
  }
}