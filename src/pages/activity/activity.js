import D3 from 'd3';
import C3 from 'c3';
import Axios from 'axios';

export class Activity {

  constructor() {
    const now = new Date();
    this.to = now.toISOString();
    // this.to = "2016-05-29T14:15:27.013Z";
    now.setDate(now.getDate() - 1);
    this.from = now.toISOString();
    // this.from = "2016-05-28T14:15:27.013Z";
    this.interval = 'hour';
    this.previousFrom;
    this.previousTo;
    this.previousInterval;
    this.baseUrl = 'http://localhost:3000';
    this.activityData = [];
    this.speedData = [];
    this.hosts = [];
    this.hostIndex = 0;
    this.hostType = 0;
    this.loaderA = false;
    this.loaderS = false;
    this.loaderR = false;
    this.minActivity = 0;
    this.regressionSlope = '';
    this.inverted = true;


  }

  async attached() {
    this.load();
  }

  async load(nested) {
    this.divWidth = parseInt(d3.select('.left-center').style('width'));
    this.divHeight = parseInt(d3.select('.left-center').style('height')) / 1.7;

    this.margin = {
      top: 20,
      right: 45,
      bottom: 70,
      left: 75
    };
    this.width = this.divWidth - this.margin.left - this.margin.right;
    this.height = this.divHeight - this.margin.top - this.margin.bottom;

    console.log("Inverted:" + this.inverted);
    this.loaderA = true;
    this.loaderS = true;
    D3.select("#chart").html("");
    D3.select("#linechart").html("");

    console.log(this.from)
    if (!nested || this.interval !== 'hour') {
      this.previousFrom = this.from
      this.previousTo = this.to;
      this.previousInterval = this.interval
    }

    try {
      console.log(this.to)
      console.log(this.from)
      await this.loadActivity(nested);
      if (this.activityData.length > 0) {
        this.loaderA = false;
        this.renderActivity();
      } else {

      }
    } catch (err) {
      console.log(err);
    }

    // try {
    //   await this.loadSpeed(nested);
    //   this.loaderS = false;
    //   this.renderSpeed();
    // } catch (err) {
    //   console.log(err);
    // }
    this.loadRegression();
  }


  async back() {
    this.to = this.previousTo;
    this.from = this.previousFrom;
    this.interval = this.previousInterval
    this.load(true);
  }

  async loadActivity(nested) {
    const url = this.baseUrl + '/es/transfer/speed?from=' + this.from + '&to=' + this.to + '&interval=' + this.interval;
    console.log(url);
    console.log('starting! (Activity)');
    const response = await Axios.get(url, {
      timeout: 600000
    });

    if (!nested) {
      this.hosts = response.data.map(function(element) {

        let ipHost;
        switch (element.key) {
          case "194.171.96.203":
          ipHost = "oliebol-02";
          break;
          case "194.171.96.210":
          ipHost = "oliebol-09";
          break;
          case "194.171.96.208":
          ipHost = "oliebol-07";
          break;
          case "194.171.96.209":
          ipHost = "oliebol-08";
          break;
          case "194.171.96.205":
          ipHost = "oliebol-04";
          break;
          case "194.171.96.206":
          ipHost = "oliebol-05";
          break;
          case "194.171.96.202":
          ipHost = "oliebol-01";
          break;
          case "194.171.96.204":
          ipHost = "oliebol-03";
          break;
          case "194.171.96.207":
          ipHost = "oliebol-06";
          break;
          case "194.171.96.216":
          ipHost = "haas-01";
          break;
          case "194.171.96.217":
          ipHost = "haas-02";
          break;
          case "194.171.96.173":
          ipHost = "strijker-01";
          break;
          case "194.171.96.190":
          ipHost = "strijker-18";
          break;
          case "194.171.96.188":
          ipHost = "strijker-16";
          break;
          case "194.171.96.191":
          ipHost = "strijker-19";
          break;
          case "194.171.96.174":
          ipHost = "strijker-02";
          break;
          case "194.171.96.189":
          ipHost = "strijker-17";
          break;
          case "194.171.96.176":
          ipHost = "strijker-04";
          break;
          case "194.171.96.175":
          ipHost = "strijker-03";
          break;
          case "194.171.96.178":
          ipHost = "strijker-06";
          break;
          case "194.171.96.177":
          ipHost = "strijker-05";
          break;
          case "194.171.96.187":
          ipHost = "strijker-15";
          break;
          case "194.171.96.179":
          ipHost = "strijker-07";
          break;
          case "194.171.96.182":
          ipHost = "strijker-10";
          break;
          case "194.171.96.180":
          ipHost = "strijker-08";
          break;
          case "194.171.96.184":
          ipHost = "strijker-12";
          break;
          case "194.171.96.183":
          ipHost = "strijker-11";
          break;
          case "194.171.96.186":
          ipHost = "strijker-14";
          break;
          case "194.171.96.181":
          ipHost = "strijker-09";
          break;
          case "194.171.96.185":
          ipHost = "strijker-13";
          break;
          default:
          ipHost = "Unknown Host";
        }

        const row = [element.key, ipHost, element.doc_count];
        return row;
      });
    }

    this.activityData = response.data;
    // console.log(this.hosts);
    console.log(this.activityData);
  }

  async loadSpeed(nested) {
    const url = this.baseUrl + '/es/transfer/speed?from=' + this.from + '&to=' + this.to
     + '&interval=' + this.interval;
    console.log(url);
    console.log('starting! (Speed)');
    const response = await Axios.get(url, {
      timeout: 600000
    });
    console.log(response);
    this.speedData = response.data;
    // console.log(this.hosts);
    console.log(this.speedData);
  }


  async loadRegression() {
    this.errorMessage = "";
    this.loaderR = true;
    console.log('loading regression!');
    const url = `${this.baseUrl}/es/transfer/speed/${this.hosts[this.hostIndex][0]}/lregression?from=${this.from}&to=${this.to}&interval=${this.interval}&min_activity=${this.minActivity}`;
    try {
      const response = await Axios.get(url);
      this.regressionSlope = response.data.slope;
      this.loaderR = false;
    } catch (e) {
      console.log('ssomething went horribly wrong!!');
      this.errorMessage = "date/time range or minimum activity yielded no result";
      this.loaderR = false;
    }
  }

  async changeHost(hostIndex) {
    this.hostIndex = hostIndex;
    this.load(true);
  }

  async changeHostType(hostType) {
    this.hostType = hostType;
    console.log(hostType);
  }

  toggleInverted() {
    if(this.inverted){
      this.inverted = false;
    } else {
      this.inverted = true;
    }
    console.log(this.inverted);
  }

  changeInterval(interval){
    console.log(interval);
    this.interval = interval
  }

  renderActivity() {
    const data = this.activityData[this.hostIndex].per_hour_agg.buckets;
    const maxValue = d3.max(data, function(d) { return d.speed_agg.value; });

    console.log(data.length);

    this.width = data.length * 27 - this.margin.left - this.margin.right;
    //const barWidth = 450 / data.length + 0.9;
    const barWidth = 20;

    console.log(maxValue);
    const margin = this.margin;
    const width = this.width;
    const height = this.height;


    console.log(data.length);
    // Over welke lengte moeten de bars verspreid worden
    const x = D3.scale.ordinal()
    .rangeBands([0, width], .1);

    // De maximale grootte van een bar gelijk maken aan de lengte 
    const y = D3.scale.linear()
    .range([height, 0]);
    const y1 = d3.scale.linear()
    .range([height, 0]);

    // de x axis wordt aangemaakt
    const xAxis = D3.svg.axis()
    .scale(x)
    .orient("bottom")

    if (this.interval == 'hour') {
      xAxis.tickFormat(D3.time.format("%H"));
    } else if (this.interval == 'minute') {
      xAxis.tickFormat(D3.time.format("%H:%M"));
    }

    // de y axis
    const yAxisLeft = D3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(10);

    var yAxisRight = D3.svg.axis()
    .scale(y1)
    .orient("right")
    .ticks(10); 

    // Hier wordt de buitenste container aangemaakt
    const svg = D3.select("#chart").html("")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const tooltip = D3.select("#chart")
    .append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

    const parseDate = D3.time.format("%Y-%m-%dT%H:%M:%S.%LZ").parse;

    data.map(function(d) {
      d.date = parseDate(d.key_as_string);
    });
    
    const line = d3.svg.line()
    .interpolate("basis")
    .x(function(d) { return x(d.date); })

    if(!this.inverted){
      line.y(function(d) { 
        return y1(d.speed_agg.value); });
    } else {
      line.y(function(d) {
        return y1(d.speed_agg.value); });
    }

    // temp kleur voor het veranderen van de kleur
    let tempColor;

    // Verschillende soorten
    const colors = D3.scale.linear()
    .domain([0, data.length * .33, data.length * .66, data.length])
    .range(['#B58929', '#C61C6F', '#268BD2', '#85992C'])

    x.domain(data.map(function(d) {
      return d.date;
    }));
    
    // Hier geef je aan wat de minimale bar is en de grootste
    y.domain([0, D3.max(data, function(d) {
      return d.doc_count;
    })]);

    if(!this.inverted){
      y1.domain([0, D3.max(data, function(d) {
        return d.speed_agg.value;
      })]);
    } else {
      y1.domain([D3.max(data, function(d) {
        return d.speed_agg.value;
      }), 0]);
    }

    // Hier wordt de x axis toegevoegd
    svg.append("g")
    .attr("class", "x axis")
    .attr("id", "xaxis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .selectAll("text")
    .attr("y", 0)
    .attr("x", 15)
    .attr("dy", ".35em")
    .attr("transform", "rotate(80)")
    .style("text-anchor", "start");

    // y axis
    svg.append("g")
    .attr("class", "y axis")
    .attr("id", "yaxis")
    .call(yAxisLeft)
    .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("# transfers");

    svg.append("g")
    .attr("class", "y axis")
    .attr("id", "yaxis")
    .attr("transform", "translate(" + width + " ,0)") 
    .style("fill", "steelblue")   
    .call(yAxisRight)
    .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", -20)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("Average Speed");


    if (this.interval == 'hour' || this.interval == 'minute') {
      // de bars in de chart
      const bars = svg.selectAll(".bar")
      .data(data)
      .enter().append("rect")
      .attr("class", "bar")
      .style('fill', function(d, i) {
        return colors(i);
      })
      .attr("x", function(d) {
        return x(d.date);
      })
      .attr("width", barWidth)
      .attr("y", height)
      .attr("height", 0)

      .on('click', (d) => {
        let newFrom;
        if (this.interval == 'hour') {
          newFrom = D3.time.hour.offset(d.date, 1);
          this.interval = 'minute';
        } else {
          newFrom = D3.time.hour.offset(d.date, 1);
        }
        // } else if (this.interval == 'minute') {
        //     // var newDate = D3.time.minute.offset(d.key_as_string, 1);
        //     // barChart.interval = 'second';
        // }

        const formatDate = D3.time.format("%Y-%m-%dT%H:%M:%S.%LZ");

        this.from = formatDate(d.date);
        this.to = formatDate(newFrom);

        this.load(true);
      })

      .on('mouseover', function(d) {
        tooltip.transition()    
        .duration(200)    
        .style("opacity", 1);    
        tooltip.html('Doc Count: ' + d.doc_count + '<br/> Average Speed: ' + parseFloat(d.speed_agg.value).toFixed(2))  
        .style("left", (d3.event.pageX) + "px")   
        .style("top", (d3.event.pageY - 175) + "px");

        tempColor = this.style.fill;
        D3.select(this)
        .style('opacity', .5)
        .style('fill', 'yellow')
      })

      .on('mouseout', function(d) {

        tooltip.html();
        tooltip.transition()
        .duration(200)
        .style('opacity', 0);
        
        D3.select(this)
        .style('opacity', 1)
        .style('fill', tempColor)
      })

      .transition()
      .attr("y", function(d) {
        return y(d.doc_count);
      })
      .attr("height", function(d) {
        return height - y(d.doc_count);
      })
      if(data.length < 61){
        bars.delay(function(d, i) {
          return i * 50;
        })
      } else {
        bars.delay(function(d, i) {
          return i * 1.5;
        })
      }

      bars.duration(1000)
      .ease('elastic')

      svg.append("path")
      .datum(data)
      .attr("class", "line")
      .attr("d", line)
      .style("fill", "none") 
      .style("stroke", "steelblue")
      .style("stroke-width", "1.5px");
    }

  }

  renderSpeed() {
    const margin = this.margin;
    const width = this.width;
    const height = this.height;

    let data = this.speedData[this.hostIndex].per_hour_agg.buckets;
    const maxValue = d3.max(data, function(d) { return d.speed_agg.value; }) + 60;
    console.log("Max: " + maxValue);
    console.log(data);

    // const x = d3.time.scale()
    // .range([0, width]);

    const x = D3.scale.ordinal()
    .rangeRoundBands([0, width], .1);

    const y = d3.scale.linear()
    .range([height, 0]);

    const xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
      // .tickFormat(D3.time.format("%H"));

      if (this.interval == 'day') {

      } else if (this.interval == 'hour') {
        xAxis.tickFormat(D3.time.format("%H"));
      } else if (this.interval == 'minute') {
        xAxis.tickFormat(D3.time.format("%M"));
      }

    // if(data.length > 60) {
    //     xAxis.tickFormat(D3.time.format("%a %d")); 
    // }


    const yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

    const line = d3.svg.line()
    .x(function(d) {
      return x(d.date);
    })
    .y(function(d) {
      return y(d.speed_agg.value);
    });

    let svg = d3.select("#linechart").html("")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const colors = D3.scale.linear()
    .domain([0, data.length * .33, data.length * .66, data.length])
    .range(['#B58929', '#C61C6F', '#268BD2', '#85992C'])

    const barWidth = 450 / data.length + 0.9;

    const parseDate = D3.time.format("%Y-%m-%dT%H:%M:%S.%LZ").parse;
    data.forEach(function(d) {
      d.date = parseDate(d.key_as_string);
    });

    x.domain(data.map(function(d) {
      return d.date;
    }));
    //x.domain(d3.extent(data, function(d) { return d.date; }));
    y.domain(d3.extent(data, function(d) {
      return d.speed_agg.value;
    }));

    svg.append("g")
    .attr("class", "x axis")
    .attr("id", "xaxis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .selectAll("text")
    .attr("y", 0)
    .attr("x", 9)
    .attr("dy", ".35em")
    .attr("transform", "rotate(80)")
    .style("text-anchor", "start");


    svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("Average Speed");

    const bars = svg.selectAll(".bar")
    .data(data)
    .enter().append("rect")
    .attr("class", "bar")
    .style('fill', function(d, i) {
      return colors(i);
    })
    .attr("x", function(d) {
      return x(d.date);
    })
    .attr("width", barWidth)

    if(this.inverted){
      bars.attr("y", function(d) {
        console.log(maxValue - d.speed_agg.value);
        return y(maxValue - d.speed_agg.value);
      })
      .attr("height", function(d) {
        return height - y(maxValue - d.speed_agg.value);
      })
    } else {
      bars.attr("y", function(d) {
        console.log(d.speed_agg.value)
        return y(d.speed_agg.value);
      })
      .attr("height", function(d) {
        return height - y(d.speed_agg.value);
      })
    }

    // svg.append("path")
    // .datum(data)
    // .attr("class", "line")
    // .attr("d", line);
  }
}