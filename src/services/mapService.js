import {inject} from 'aurelia-dependency-injection';
import {Config} from '../config';
import Axios from 'axios';

@inject(Config)
export class MapService {
	hosts = [];
	stores;
	retrieves;
	currentLogs;

	constructor(config) {
		this.config = config;
		this.baseUrl = config.get('apiUrl');
	}

	async loadLogs(from, to) {
		return new Promise(async(resolve, reject) => {
			try {
				const storRes = await Axios.get(`${this.baseUrl}/es/transfer/stores?from=${from}&to=${to}`,
				{
					timeout: 12000000
				});

				const retRes = await Axios.get(`${this.baseUrl}/es/transfer/retrieves?from=${from}&to=${to}`,
				{
					timeout: 12000000
				});

				if (storRes.status == 200 && retRes.status == 200) {

					this.stores = storRes.data;
					this.retrieves = retRes.data;

					for (var key in this.retrieves.hosts[1]) {
						this.hosts.push({
							key: key
						});
					}

					resolve(this.hosts);
				} else {
					//Axios error message? Which request failed?
					reject('Request failed!');
				}
			} catch (e) {
				reject(e);
			}
		});
	}

	getHostLogs(type, index) {
		if (type === 0) {
			this.currentLogs = this.stores.hosts[1][this.hosts[index].key];
			return this.currentLogs;
		} else {
			this.currentLogs = this.retrieves.hosts[1][this.hosts[index].key];
			return this.currentLogs;
		}
	}


	getLocLogs(type) {
		if (type === 0) {
			this.currentLogs = this.stores.locs;
			return this.currentLogs;
		} else {
		  this.currentLogs = this.retrieves.locs;
		  return this.currentLogs;
		}
	}


	getMaxFreq(list) {
		let maxFreq = 0;
		if (list) {
			this.currentLogs.map((elem) => {
				if (elem.frequency > maxFreq) {
					maxFreq = elem.frequency;
				}
			});
		} else {
			for (const key in this.currentLogs) {
				if (this.currentLogs[key].frequency > maxFreq) {
					maxFreq = this.currentLogs[key].frequency;
				}
			}
		}
		return maxFreq;
	}

	getMaxFileSize(list) {
		let maxSize = 0;
		if (list) {
			this.currentLogs.map((elem) => {
				if (elem.totalSize > maxSize) {
					maxSize = elem.totalSize;
				}
			});
		} else {
			for (const key in this.currentLogs) {
				if (this.currentLogs[key].totalSize > maxSize) {
					maxSize = this.currentLogs[key].totalSize;
				}
			}
		}
		return maxSize;
	}


	calcFreqColor(frequency, maxFreq) {
		let color;
		if (frequency < (maxFreq * 0.03)) {
			color = "#008000";
		} else if (frequency >= (maxFreq * 0.03) && frequency < (maxFreq * 0.06)) {
			color = "#368e00";
		} else if (frequency >= (maxFreq * 0.06) && frequency < (maxFreq * 0.09)) {
			color = "#549b00";
		} else if (frequency >= (maxFreq * 0.09) && frequency < (maxFreq * 0.12)) {
			color = "#6ca700";
		} else if (frequency >= (maxFreq * 0.12) && frequency < (maxFreq * 0.15)) {
			color = "#82b100";
		} else if (frequency >= (maxFreq * 0.15) && frequency < (maxFreq * 0.18)) {
			color = "#96b900";
		} else if (frequency >= (maxFreq * 0.18) && frequency < (maxFreq * 0.21)) {
			color = "#a8c000";
		} else if (frequency >= (maxFreq * 0.21) && frequency < (maxFreq * 0.24)) {
			color = "#b8c500";
		} else if (frequency >= (maxFreq * 0.24) && frequency < (maxFreq * 0.27)) {
			color = "#c7c900";
		} else if (frequency >= (maxFreq * 0.27) && frequency < (maxFreq * 0.3)) {
			color = "#d4cb00"
		} else if (frequency >= (maxFreq * 0.3) && frequency < (maxFreq * 0.33)) {
			color = "#dfcb00"
		} else if (frequency >= (maxFreq * 0.33) && frequency < (maxFreq * 0.36)) {
			color = "#e9ca00"
		} else if (frequency >= (maxFreq * 0.36) && frequency < (maxFreq * 0.39)) {
			color = "#f1c700"
		} else if (frequency >= (maxFreq * 0.39) && frequency < (maxFreq * 0.42)) {
			color = "#f7c200"
		} else if (frequency >= (maxFreq * 0.42) && frequency < (maxFreq * 0.45)) {
			color = "#fcbc00"
		} else if (frequency >= (maxFreq * 0.45) && frequency < (maxFreq * 0.48)) {
			color = "#feb400"
		} else if (frequency >= (maxFreq * 0.48) && frequency < (maxFreq * 0.51)) {
			color = "#ffaa00"
		} else if (frequency >= (maxFreq * 0.51) && frequency < (maxFreq * 0.54)) {
			color = "#ff9f1c"
		} else if (frequency >= (maxFreq * 0.54) && frequency < (maxFreq * 0.57)) {
			color = "#ff9434"
		} else if (frequency >= (maxFreq * 0.57) && frequency < (maxFreq * 0.60)) {
			color = "#ff8842"
		} else if (frequency >= (maxFreq * 0.60) && frequency < (maxFreq * 0.63)) {
			color = "#fd7d4b"
		} else if (frequency >= (maxFreq * 0.63) && frequency < (maxFreq * 0.66)) {
			color = "#f97152"
		} else if (frequency >= (maxFreq * 0.66) && frequency < (maxFreq * 0.69)) {
			color = "#f56656"
		} else if (frequency >= (maxFreq * 0.69) && frequency < (maxFreq * 0.72)) {
			color = "#f05b57"
		} else if (frequency >= (maxFreq * 0.72) && frequency < (maxFreq * 0.75)) {
			color = "#e95057"
		} else if (frequency >= (maxFreq * 0.75) && frequency < (maxFreq * 0.88)) {
			color = "#e24555"
		} else if (frequency >= (maxFreq * 0.88) && frequency < (maxFreq * 0.91)) {
			color = "#da3a50"
		} else if (frequency >= (maxFreq * 0.91) && frequency < (maxFreq * 0.94)) {
			color = "#d12f4b"
		} else if (frequency >= (maxFreq * 0.94) && frequency < (maxFreq * 0.97)) {
			color = "#c82543"
		} else if (frequency >= (maxFreq * 0.97) && frequency < (maxFreq * 0.99)) {
			color = "#bd1b3a"
		} else if (frequency >= (maxFreq * 0.99) && frequency <= maxFreq) {
			color = "#8b0000";
		}
		return color;
	}

	calcFileSizeColor(filesize, maxSize) {
		let color;
		if (filesize < (maxSize * 0.03)) {
			color = "#008000";
		} else if (filesize >= (maxSize * 0.03) && filesize < (maxSize * 0.06)) {
			color = "#368e00";
		} else if (filesize >= (maxSize * 0.06) && filesize < (maxSize * 0.09)) {
			color = "#549b00";
		} else if (filesize >= (maxSize * 0.09) && filesize < (maxSize * 0.12)) {
			color = "#6ca700";
		} else if (filesize >= (maxSize * 0.12) && filesize < (maxSize * 0.15)) {
			color = "#82b100";
		} else if (filesize >= (maxSize * 0.15) && filesize < (maxSize * 0.18)) {
			color = "#96b900";
		} else if (filesize >= (maxSize * 0.18) && filesize < (maxSize * 0.21)) {
			color = "#a8c000";
		} else if (filesize >= (maxSize * 0.21) && filesize < (maxSize * 0.24)) {
			color = "#b8c500";
		} else if (filesize >= (maxSize * 0.24) && filesize < (maxSize * 0.27)) {
			color = "#c7c900";
		} else if (filesize >= (maxSize * 0.27) && filesize < (maxSize * 0.3)) {
			color = "#d4cb00"
		} else if (filesize >= (maxSize * 0.3) && filesize < (maxSize * 0.33)) {
			color = "#dfcb00"
		} else if (filesize >= (maxSize * 0.33) && filesize < (maxSize * 0.36)) {
			color = "#e9ca00"
		} else if (filesize >= (maxSize * 0.36) && filesize < (maxSize * 0.39)) {
			color = "#f1c700"
		} else if (filesize >= (maxSize * 0.39) && filesize < (maxSize * 0.42)) {
			color = "#f7c200"
		} else if (filesize >= (maxSize * 0.42) && filesize < (maxSize * 0.45)) {
			color = "#fcbc00"
		} else if (filesize >= (maxSize * 0.45) && filesize < (maxSize * 0.48)) {
			color = "#feb400"
		} else if (filesize >= (maxSize * 0.48) && filesize < (maxSize * 0.51)) {
			color = "#ffaa00"
		} else if (filesize >= (maxSize * 0.51) && filesize < (maxSize * 0.54)) {
			color = "#ff9f1c"
		} else if (filesize >= (maxSize * 0.54) && filesize < (maxSize * 0.57)) {
			color = "#ff9434"
		} else if (filesize >= (maxSize * 0.57) && filesize < (maxSize * 0.60)) {
			color = "#ff8842"
		} else if (filesize >= (maxSize * 0.60) && filesize < (maxSize * 0.63)) {
			color = "#fd7d4b"
		} else if (filesize >= (maxSize * 0.63) && filesize < (maxSize * 0.66)) {
			color = "#f97152"
		} else if (filesize >= (maxSize * 0.66) && filesize < (maxSize * 0.69)) {
			color = "#f56656"
		} else if (filesize >= (maxSize * 0.69) && filesize < (maxSize * 0.72)) {
			color = "#f05b57"
		} else if (filesize >= (maxSize * 0.72) && filesize < (maxSize * 0.75)) {
			color = "#e95057"
		} else if (filesize >= (maxSize * 0.75) && filesize < (maxSize * 0.88)) {
			color = "#e24555"
		} else if (filesize >= (maxSize * 0.88) && filesize < (maxSize * 0.91)) {
			color = "#da3a50"
		} else if (filesize >= (maxSize * 0.91) && filesize < (maxSize * 0.94)) {
			color = "#d12f4b"
		} else if (filesize >= (maxSize * 0.94) && filesize < (maxSize * 0.97)) {
			color = "#c82543"
		} else if (filesize >= (maxSize * 0.97) && filesize < (maxSize * 0.99)) {
			color = "#bd1b3a"
		} else if (filesize >= (maxSize * 0.99) && filesize <= maxSize) {
			color = "#8b0000";
		}
		return color;
	}
}
